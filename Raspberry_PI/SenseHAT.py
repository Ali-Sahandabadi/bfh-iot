import os
import time

from sense_hat import SenseHat

from socketIO_client import SocketIO, LoggingNamespace

print('START')

sense = SenseHat()
socketIO = SocketIO('https://bfh-rasp.herokuapp.com', verify=False)

def get_cpu_temp():
	res = os.popen("vcgencmd measure_temp").readline()
	t = float(res.replace("temp=","").replace("'C\n",""))
  	return(t)

# use moving average to smooth readings
def get_smooth(x):
  if not hasattr(get_smooth, "t"):
    get_smooth.t = [x,x,x]
  get_smooth.t[2] = get_smooth.t[1]
  get_smooth.t[1] = get_smooth.t[0]
  get_smooth.t[0] = x
  xs = (get_smooth.t[0]+get_smooth.t[1]+get_smooth.t[2])/3
  return(xs)

while True:

  # Take readings from all three sensors
  #t = sense.get_temperature()
  t1 = sense.get_temperature_from_humidity()
  t2 = sense.get_temperature_from_pressure()
  t_cpu = get_cpu_temp()
  p = sense.get_pressure()
  h = sense.get_humidity()

  # calculates the real temperature compesating CPU heating
  t = (t1+t2)/2
  t_corr = t - ((t_cpu-t)/1.5)
  t_corr = float("{0:.1f}".format(get_smooth(t_corr)))

  # Round the values to one decimal place
  t = round(t_corr, 1)
  p = round(p, 1)
  h = round(h, 1)

  socketIO.emit('temperature',str(t_corr))
  socketIO.emit('pressure',str(p))
  socketIO.emit('humidity',str(h))
  socketIO.wait(seconds=1)

  # Create the message
  # str() converts the value to a string so it can be concatenated
  #message = "Temperature: " + str(t_corr) + " Pressure: " + str(p) + " Humidity: " + str(h)

  # Display the scrolling message on LED
  #sense.show_message(message, scroll_speed=0.05)
