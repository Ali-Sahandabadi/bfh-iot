package ch.bfh.bfhpi;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class TempratureFragment extends Fragment {

    private View mView;

    private TextView test;
    private Socket mSocket;
    private GraphView graph;
    int i = 2;
    private LineGraphSeries<DataPoint> mSeries1;
    private double graph2LastXValue = 5d;
    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_temprature, container, false);


        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        test = mView.findViewById(R.id.TestTemp);
        SocketConnection app = (SocketConnection) getActivity().getApplication();
        mSocket = app.getSocket();
        mSocket.on("temperature", onNewMessage_temperature);
        test.setText("Temperature is not available!");
        mSocket.connect();
        graph = (GraphView) mView.findViewById(R.id.graphtemp);

        graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
        graph.getViewport().setScalableY(true); // enables vertical zooming and scrolling
        // set manual X bounds
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-10);
        graph.getViewport().setMaxY(100);

        mSeries1 = new LineGraphSeries<>();
        graph.addSeries(mSeries1);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
    }

    private Emitter.Listener onNewMessage_temperature = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String data = (String) args[0];
                    test.setText(data);
                    graph2LastXValue += 1d;
                    mSeries1.appendData(new DataPoint(graph2LastXValue, Double.parseDouble(data)), true, 20);
                    i++;

                }
            });
        }
    };


}

