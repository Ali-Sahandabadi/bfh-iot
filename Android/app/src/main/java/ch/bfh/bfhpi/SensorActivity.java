package ch.bfh.bfhpi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class SensorActivity extends AppCompatActivity {

    private boolean isStartup = true;
    private Fragment fragmentTemprature = new TempratureFragment();
    private Fragment fragmentPressure = new PressureFragment();
    private Fragment fragmentHumidity = new HumidityFragment();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            if (isStartup) {
                ((FrameLayout) findViewById(R.id.fragment_switch)).removeAllViews();
                isStartup = false;
            }


            switch (item.getItemId()) {
                case R.id.navigation_temperature:

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_switch, fragmentTemprature);
                    ft.commit();

                    return true;
                case R.id.navigation_pressure:


                    FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                    ft1.replace(R.id.fragment_switch, fragmentPressure);
                    ft1.commit();
                    return true;

                case R.id.navigation_humidity:
                    FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();
                    ft2.replace(R.id.fragment_switch, fragmentHumidity);
                    ft2.commit();
                    return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_switch, fragmentTemprature);
        ft.commit();

        BottomNavigationView navigation = findViewById(R.id.navigation_sensor);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
